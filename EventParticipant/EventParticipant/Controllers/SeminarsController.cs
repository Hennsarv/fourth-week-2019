﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventParticipant.Models;

namespace EventParticipant.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}

namespace EventParticipant.Controllers
{
    public class SeminarsController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Seminars
        public ActionResult Index()
        {
            return View(db.Seminar.ToList());
        }

        // GET: Seminars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seminar seminar = db.Seminar.Find(id);
            if (seminar == null)
            {
                return HttpNotFound();
            }
            ViewBag.paymentId = new SelectList(db.PaymentType, "Id", "Name");
            ViewBag.personId = new SelectList(db.Person, "Id", "FullName");
            return View(seminar);
        }

        public ActionResult AddAttendee(int id, int? personId, int? paymentId)
        {
            if (personId == null || paymentId == null)
            {
                TempData["ErrorMessage"] = "vali isik JA makseviis";
                return RedirectToAction("Details", new { id }); 
            }
            Person person = db.Person.Find(personId.Value);
            Seminar seminar = db.Seminar.Find(id);
            if (person != null && seminar != null)
            {
                try
                {
                    seminar.Participant.Add(new Participant { Person = person, MakseViisId = paymentId.Value });
                    db.SaveChanges();    
                }
                catch { }
            }

            return RedirectToAction("Details", new { id });
        }
        public ActionResult RemoveAttendee(int id, int personId)
        {
            Participant participant = db.Participant.Where(x => x.SeminarId == id && x.PersonId == personId).FirstOrDefault();
            if (participant != null)
            {
                try
                {
                    db.Participant.Remove(participant);
                    db.SaveChanges();
                }
                catch { }
            }

            return RedirectToAction("Details", new { id });
        }
        // GET: Seminars/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Seminars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Seminar seminar)
        {
            if (ModelState.IsValid)
            {
                db.Seminar.Add(seminar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(seminar);
        }

        // GET: Seminars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seminar seminar = db.Seminar.Find(id);
            if (seminar == null)
            {
                return HttpNotFound();
            }
            return View(seminar);
        }

        // POST: Seminars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Seminar seminar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seminar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(seminar);
        }

        // GET: Seminars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seminar seminar = db.Seminar.Find(id);
            if (seminar == null)
            {
                return HttpNotFound();
            }
            return View(seminar);
        }

        // POST: Seminars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Seminar seminar = db.Seminar.Find(id);
            db.Seminar.Remove(seminar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
