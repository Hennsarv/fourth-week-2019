﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega;

namespace MVCAutentimisega
{
    partial class Album
    {
        public int OrderNo
        {
            get => OrderId == 0 ? Id : OrderId;
            set => OrderId = value;
        }
    }
}

namespace MVCAutentimisega.Controllers
{
    public class AlbumsController : Controller
    {
        private Entities db = new Entities();

        // GET: Albums
        public ActionResult Index()
        {
            var albums = db.Albums.Include(a => a.AspNetUser).ToList();
            return View(albums.OrderBy(x => x.OrderNo).ToList());
        }

        public ActionResult MoveUp(int id)
        {
            Album a1 = db.Albums.Find(id);
            if (a1 != null)
            {
                Album a2 = db.Albums.ToList().Where(x => x.OrderNo < a1.OrderNo).OrderBy(x => x.OrderNo).LastOrDefault();
                if (a2 != null)
                {
                    int uus = a2.OrderNo;
                    a2.OrderNo = a1.OrderNo;
                    a1.OrderNo = uus;
                    db.SaveChanges();
                }

            }

            return RedirectToAction("Index");
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // GET: Albums/Create
        public ActionResult Create()
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Index");
            AspNetUser user = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).Single();
            

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", user.Id);
            return View(new Album { UserId = user.Id });
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrderId,UserId,Name,Description,Visibility,CoverPictureId")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Albums.Add(album);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", album.UserId);
            return View(album);
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", album.UserId);
            return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderId,UserId,Name,Description,Visibility,CoverPictureId")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", album.UserId);
            return View(album);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
