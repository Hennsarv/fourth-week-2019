﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaKordus.Models;

namespace MVCAutentimisegaKordus.Models
{
    partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}

namespace MVCAutentimisegaKordus.Controllers
{
    public class PeopleController : Controller
    {
        private AabitsEntities db = new AabitsEntities();

        // GET: People
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                Person person = db.People.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                if (person != null)
                {
                    string fullname = person?.FullName ?? "tundmatu";
                    //fullname = person == null ? "tundmatu" : person.FullName;
                    ViewBag.FullName = fullname;

                    if (person.IsTeacher)
                    {
                        var people = db.People.Include(p => p.StudentGroup);
                        return View(people.ToList());
                    }
                    else
                    {
                        var people = person.StudentGroup?.People.ToList();
                        return View(people.ToList());
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.StudentGroups, "Id", "Name");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,IK,IsTeacher,Email,PictureId,GroupId")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.StudentGroups, "Id", "Name", person.GroupId);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.StudentGroups, "Id", "Name", person.GroupId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,IK,IsTeacher,Email,PictureId,GroupId")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.StudentGroups, "Id", "Name", person.GroupId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
