﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SortimisDemo.Models;

namespace SortimisDemo.Controllers
{
    public class ProductsController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Products
        public ActionResult Index(string sortKey = "ProductId", string sortDirection = "Asc", string searchString = "", decimal lowerPrice = 0, decimal higherPrice = 0, int CategoryId = 0)
        {
            var q = db.Products
                .Where(x => searchString=="" || x.ProductName.Contains(searchString))
                .Where(x => lowerPrice == 0 || x.UnitPrice >= lowerPrice)
                .Where(x => higherPrice == 0 || x.UnitPrice <= higherPrice)
                .Where(x => CategoryId == 0 || x.CategoryID == CategoryId)
                ;
            var qs = q.OrderBy(x => x.ProductID);

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", CategoryId);


            ViewBag.SortKey = sortKey;
            ViewBag.SortDirection = sortDirection;
            ViewBag.SearchString = searchString;
            ViewBag.LowerPrice = lowerPrice;
            ViewBag.HigherPrice = higherPrice;

            switch (sortKey + sortDirection)
            {
                case "ProductNameAsc": qs = q.OrderBy(x => x.ProductName); break;
                case "ProductNameDesc": qs = q.OrderByDescending(x => x.ProductName); break;
                case "UnitPriceAsc": qs = q.OrderBy(x => x.UnitPrice); break;
                case "UnitPriceDesc": qs = q.OrderByDescending(x => x.UnitPrice); break;
                case "CategoryAsc": qs = q.OrderBy(x => x.CategoryID); break;
                case "CategoryDesc": qs = q.OrderByDescending(x => x.CategoryID); break;

            }
            return View(qs.ToList());

        }

        public ActionResult Filter(string sortKey = "ProductId", string sortDirection = "Asc", string searchString = "", decimal lowerPrice = 0, decimal higherPrice = 0, int pageNo = 0, int pageSize = 10)
        {
            if (pageNo < 0) pageNo = 0;
            var q = db.Products
                .Where(x => searchString == "" || x.ProductName.Contains(searchString))
                .Where(x => lowerPrice == 0 || x.UnitPrice >= lowerPrice)
                .Where(x => higherPrice == 0 || x.UnitPrice <= higherPrice)
                ;

            

            var qs = q.OrderBy(x => x.ProductID);

            ViewBag.SortKey = sortKey;
            ViewBag.SortDirection = sortDirection;
            ViewBag.SearchString = searchString;
            ViewBag.LowerPrice = lowerPrice;
            ViewBag.HigherPrice = higherPrice;
            ViewBag.PageNo = pageNo;
            ViewBag.PageSize = pageSize;

            switch (sortKey + sortDirection)
            {
                case "ProductNameAsc": qs = q.OrderBy(x => x.ProductName); break;
                case "ProductNameDesc": qs = q.OrderByDescending(x => x.ProductName); break;
                case "UnitPriceAsc": qs = q.OrderBy(x => x.UnitPrice); break;
                case "UnitPriceDesc": qs = q.OrderByDescending(x => x.UnitPrice); break;
                case "CategoryAsc": qs = q.OrderBy(x => x.CategoryID); break;
                case "CategoryDesc": qs = q.OrderByDescending(x => x.CategoryID); break;

            }
            return View(qs
                .Skip(pageNo*pageSize).Take(pageSize)
                .ToList());

        }


        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
