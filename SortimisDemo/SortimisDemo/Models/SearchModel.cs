﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SortimisDemo.Models
{
    public class SearchModel
    {
        public string sortKey { get; set; } = "ProductName";
        public string sortDirection { get; set; } = "Asc";
        public decimal lowerPrice { get; set; } = 0;
        public decimal higherPrice { get; set; } = 0;
        public string searchString { get; set; } = "";
    }
}